package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    
    public void run() {
        
        final Map<String, String> WIN_MAP = Map.of(
            "rock", "scissors",
            "paper", "rock",
            "scissors", "paper"
        );
        
        while(true) {
            System.out.printf("Let's play round %d\n", roundCounter ++);

            String computer_choice = rpsChoices.get(randInt(3));
            String human_choice = constrainedInput(
                rpsChoices, "Your choice (Rock/Paper/Scissors)?"
            );
            
            String result_message;
            if( WIN_MAP.get(human_choice).equals(computer_choice) ) {
                humanScore ++;
                result_message = "Human wins!";
            } else if( WIN_MAP.get(computer_choice).equals(human_choice) ) {
                computerScore ++;
                result_message = "Computer wins!";
            } else {
                result_message = "It's a tie!";
            }
            
            System.out.printf( // Round summary
                "Human chose %s, computer chose %s. %s\nScore: human %d, computer %d\n",
                human_choice, computer_choice, result_message, humanScore, computerScore
            );
            
            String continue_playing = constrainedInput(
                List.of("y", "n"), "Do you wish to continue playing? (y/n)?"
            );

            if(continue_playing.equals("n")) { break; }
        }
        System.out.println("Bye bye :)");
    }

    int randInt(int max) {
        return (int) (Math.random() * max);
    }

    String constrainedInput(List<String> valid_choices, String prompt) {
        while(true){
            String human_choice = readInput(prompt);
            if (valid_choices.contains(human_choice)){
                return human_choice;
            }
            System.out.println(
                "I do not understand " + human_choice + ". Could you try again?"
            );
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
